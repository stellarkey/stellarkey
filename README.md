# StellarKey

A DLC unlocker for Steam games on Linux. So far it's only been tested on
a few games. It should work on most games with some minor tweaking.

Download the [latest release] or compile with `make`. Then run your game with
the `LD_PRELOAD` environment variable like:

    cd /path/to/your/game
    LD_PRELOAD=/path/to/libstellarkey.so ./game

Or set Steam launch options:

    LD_PRELOAD="/path/to/libstellarkey.so:$LD_PRELOAD" %command%

Inspired by [hookey] and [CreamAPI].
Thanks to [Goldberg Emulator] for his reference implementation of the Steam API.
An open source 
Patches welcome.

[latest release]: https://0xacab.org/stellarkey/stellarkey/-/releases
[Steam Runtime SDK]: https://gitlab.steamos.cloud/steamrt/scout/sdk/
[hookey]: https://github.com/goddessfreya/hookey/
[CreamAPI]: https://cs.rin.ru/forum/viewtopic.php?f=29&t=70576
[Goldberg Emulator]: https://gitlab.com/Mr_Goldberg/goldberg_emulator

## Configuration

StellarKey will unlock all DLC for most games, but in case this auto-detection
does not work, StellarKey will look for a configuration file called `DLC.txt` or
`cream_api.ini` in the game's working directory, or inside a `steam_settings`
directory if no configuration files were found outside. The format is like this:

    12345 = "DLC Name"
    67890=Another DLC

With optional spaces and double quotes. So simple configuration files from
[Goldberg Emulator] or [CreamAPI] will work with no modifications. However,
unlike these, StellarKey does not currently look for configuration files in the
directory where `libstellarkey.so` is located.

Additionally you can set the `STELLARKEY_DIR` environment variable to make
StellarKey look for configuration files anywhere else. For example you could
set the Steam launch options like this:

    STELLARKEY_DIR="${HOME}/sk_config/${SteamAppId}/" LD_PRELOAD="/path/to/libstellarkey.so:$LD_PRELOAD" %command%

And then store the configuration files for the game with AppId `12345` in
`~/sk_config/12345/`.

## My game does not work
When reporting a game that StellarKey does not work for, please provide a log
file, a link to the game's data and instructions to make it work on the Goldberg
emulator. StellarKey will log its actions to a file if you run it with the
environment variable `STELLARKEY_LOG="/tmp/stellarkey.log"` or if you compile
it with `make clean; make debug`.

## TODO
- A few games use unorthodox ways to check for unlockables, such as 
  `GetInstalledDepots`, `IsUserInSource`, `GetAppInstallDir`, 
  `UserHasLicenseForApp`, mods, inventory, and so on. Find games that do this
  and implement these and other functions.
- Move away from `libstdc++`, it's huge and causes problems with symbol clashes.
- Somebody that actually knows C++ should take a look at this and make it more
  idiomatic.
- Unity games using [Steamworks.NET] find the SteamApps interface using `dlsym`.
  Hook `dlsym` itself to make Stellarkey work with these. A complication is that
  `dlsym` reads the stack to implement `RTLD_NEXT`, so the implementation of
  the hook will need to jump to the real `dlsym` in assembly without messing up
  the stack.
- Move over to `LD_AUDIT` instead of `LD_PRELOAD`. It looks like it would solve
  the problems with symbol clashes and `dlsym`.
- Mac OS port.
- Find a way to unlock all DLC automatically in games that query a list from 
  the Steam API. Maybe simply ask SteamDB over http.
- Support games using the Proton compatibility layer. Research how those games
  talk to the native Steam client, and if it's possible to hook wine libraries
  with LD_PRELOAD.

[Steamworks.NET]: https://github.com/rlabrecque/Steamworks.NET
