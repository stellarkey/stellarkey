version = -DSTELLARKEY_VERSION="$(shell git describe --long --always; $(CXX) --version | head -n 1)"
CXXFLAGS += -std=c++11 $(version)
EXPORT_FLAGS += -fvisibility=hidden -fwhole-program

.PHONY: stripped debug static clean release

stripped: LDFLAGS += -s -Wl,--gc-sections
stripped: static

debug: CXXFLAGS += -Og -g -DSTELLARKEY_LOG="/tmp/stellarkey.log"
debug: static

static: LDFLAGS += -Wl,--exclude-libs,ALL -static-libstdc++
static: libstellarkey.so libstellarkey32.so

%.so: LDFLAGS += -shared
%.so: CXXFLAGS += -fPIC

libstellarkey.so: stellarkey.o
	$(CXX) $(LDFLAGS) $? $(LDLIBS) -o $@

libstellarkey32.so: stellarkey32.o
	$(CXX) -m32 $(LDFLAGS) $? $(LDLIBS) -o $@

stellarkey.o: CXXFLAGS += $(EXPORT_FLAGS)
stellarkey.o: stellarkey.cpp
	$(CXX) -c $(CPPFLAGS) $(CXXFLAGS) stellarkey.cpp

stellarkey32.o: CXXFLAGS += $(EXPORT_FLAGS)
stellarkey32.o: stellarkey.cpp
	$(CXX) -m32 -c $(CPPFLAGS) $(CXXFLAGS) stellarkey.cpp -o stellarkey32.o

stellarkey.cpp: proxies.cpp

proxies.cpp:
	python3 gen_proxies_cpp.py > proxies.cpp

clean:
	rm *.o *.so

release: stripped
	zip -X StellarKey-$(shell git describe).zip *.so README.md

