// Generated with gen_proxies_cpp.py

class ProxySteamClient: public ISteamClient {
protected:
	ISteamClient* Upstream;

public:
	ProxySteamClient(ISteamClient* Upstream):
		Upstream{ Upstream }
	{}

	HSteamPipe CreateSteamPipe() override { return Upstream->CreateSteamPipe(); }
	bool BReleaseSteamPipe(HSteamPipe hSteamPipe) override { return Upstream->BReleaseSteamPipe(hSteamPipe); }
	HSteamUser ConnectToGlobalUser(HSteamPipe hSteamPipe) override { return Upstream->ConnectToGlobalUser(hSteamPipe); }
	HSteamUser CreateLocalUser(HSteamPipe * phSteamPipe, EAccountType eAccountType) override { return Upstream->CreateLocalUser(phSteamPipe, eAccountType); }
	void ReleaseUser(HSteamPipe hSteamPipe, HSteamUser hUser) override { return Upstream->ReleaseUser(hSteamPipe, hUser); }
	ISteamUser * GetISteamUser(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUser(hSteamUser, hSteamPipe, pchVersion); }
	ISteamGameServer * GetISteamGameServer(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGameServer(hSteamUser, hSteamPipe, pchVersion); }
	void SetLocalIPBinding(SteamIPAddress_t const & unIP, uint16 usPort) override { return Upstream->SetLocalIPBinding(unIP, usPort); }
	ISteamFriends * GetISteamFriends(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamFriends(hSteamUser, hSteamPipe, pchVersion); }
	ISteamUtils * GetISteamUtils(HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUtils(hSteamPipe, pchVersion); }
	ISteamMatchmaking * GetISteamMatchmaking(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMatchmaking(hSteamUser, hSteamPipe, pchVersion); }
	ISteamMatchmakingServers * GetISteamMatchmakingServers(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMatchmakingServers(hSteamUser, hSteamPipe, pchVersion); }
	void * GetISteamGenericInterface(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGenericInterface(hSteamUser, hSteamPipe, pchVersion); }
	ISteamUserStats * GetISteamUserStats(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUserStats(hSteamUser, hSteamPipe, pchVersion); }
	ISteamGameServerStats * GetISteamGameServerStats(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGameServerStats(hSteamuser, hSteamPipe, pchVersion); }
	ISteamApps * GetISteamApps(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamApps(hSteamUser, hSteamPipe, pchVersion); }
	ISteamNetworking * GetISteamNetworking(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamNetworking(hSteamUser, hSteamPipe, pchVersion); }
	ISteamRemoteStorage * GetISteamRemoteStorage(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamRemoteStorage(hSteamuser, hSteamPipe, pchVersion); }
	ISteamScreenshots * GetISteamScreenshots(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamScreenshots(hSteamuser, hSteamPipe, pchVersion); }
	ISteamGameSearch * GetISteamGameSearch(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGameSearch(hSteamuser, hSteamPipe, pchVersion); }
	void RunFrame() override { return Upstream->RunFrame(); }
	uint32 GetIPCCallCount() override { return Upstream->GetIPCCallCount(); }
	void SetWarningMessageHook(SteamAPIWarningMessageHook_t pFunction) override { return Upstream->SetWarningMessageHook(pFunction); }
	bool BShutdownIfAllPipesClosed() override { return Upstream->BShutdownIfAllPipesClosed(); }
	ISteamHTTP * GetISteamHTTP(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamHTTP(hSteamuser, hSteamPipe, pchVersion); }
	void * DEPRECATED_GetISteamUnifiedMessages(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->DEPRECATED_GetISteamUnifiedMessages(hSteamuser, hSteamPipe, pchVersion); }
	ISteamController * GetISteamController(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamController(hSteamUser, hSteamPipe, pchVersion); }
	ISteamUGC * GetISteamUGC(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUGC(hSteamUser, hSteamPipe, pchVersion); }
	ISteamAppList * GetISteamAppList(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamAppList(hSteamUser, hSteamPipe, pchVersion); }
	ISteamMusic * GetISteamMusic(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMusic(hSteamuser, hSteamPipe, pchVersion); }
	ISteamMusicRemote * GetISteamMusicRemote(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMusicRemote(hSteamuser, hSteamPipe, pchVersion); }
	ISteamHTMLSurface * GetISteamHTMLSurface(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamHTMLSurface(hSteamuser, hSteamPipe, pchVersion); }
	void DEPRECATED_Set_SteamAPI_CPostAPIResultInProcess(void ( *arg0 )(  )) override { return Upstream->DEPRECATED_Set_SteamAPI_CPostAPIResultInProcess(arg0); }
	void DEPRECATED_Remove_SteamAPI_CPostAPIResultInProcess(void ( *arg0 )(  )) override { return Upstream->DEPRECATED_Remove_SteamAPI_CPostAPIResultInProcess(arg0); }
	void Set_SteamAPI_CCheckCallbackRegisteredInProcess(SteamAPI_CheckCallbackRegistered_t func) override { return Upstream->Set_SteamAPI_CCheckCallbackRegisteredInProcess(func); }
	ISteamInventory * GetISteamInventory(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamInventory(hSteamuser, hSteamPipe, pchVersion); }
	ISteamVideo * GetISteamVideo(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamVideo(hSteamuser, hSteamPipe, pchVersion); }
	ISteamParentalSettings * GetISteamParentalSettings(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamParentalSettings(hSteamuser, hSteamPipe, pchVersion); }
	ISteamInput * GetISteamInput(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamInput(hSteamUser, hSteamPipe, pchVersion); }
	ISteamParties * GetISteamParties(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamParties(hSteamUser, hSteamPipe, pchVersion); }
	ISteamRemotePlay * GetISteamRemotePlay(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamRemotePlay(hSteamUser, hSteamPipe, pchVersion); }
	void DestroyAllInterfaces() override { return Upstream->DestroyAllInterfaces(); }
};

class ProxySteamApps: public ISteamApps {
protected:
	ISteamApps* Upstream;

public:
	ProxySteamApps(ISteamApps* Upstream):
		Upstream{ Upstream }
	{}

	bool BIsSubscribed() override { return Upstream->BIsSubscribed(); }
	bool BIsLowViolence() override { return Upstream->BIsLowViolence(); }
	bool BIsCybercafe() override { return Upstream->BIsCybercafe(); }
	bool BIsVACBanned() override { return Upstream->BIsVACBanned(); }
	char const * GetCurrentGameLanguage() override { return Upstream->GetCurrentGameLanguage(); }
	char const * GetAvailableGameLanguages() override { return Upstream->GetAvailableGameLanguages(); }
	bool BIsSubscribedApp(AppId_t appID) override { return Upstream->BIsSubscribedApp(appID); }
	bool BIsDlcInstalled(AppId_t appID) override { return Upstream->BIsDlcInstalled(appID); }
	uint32 GetEarliestPurchaseUnixTime(AppId_t nAppID) override { return Upstream->GetEarliestPurchaseUnixTime(nAppID); }
	bool BIsSubscribedFromFreeWeekend() override { return Upstream->BIsSubscribedFromFreeWeekend(); }
	int GetDLCCount() override { return Upstream->GetDLCCount(); }
	bool BGetDLCDataByIndex(int iDLC, AppId_t * pAppID, bool * pbAvailable, char * pchName, int cchNameBufferSize) override { return Upstream->BGetDLCDataByIndex(iDLC, pAppID, pbAvailable, pchName, cchNameBufferSize); }
	void InstallDLC(AppId_t nAppID) override { return Upstream->InstallDLC(nAppID); }
	void UninstallDLC(AppId_t nAppID) override { return Upstream->UninstallDLC(nAppID); }
	void RequestAppProofOfPurchaseKey(AppId_t nAppID) override { return Upstream->RequestAppProofOfPurchaseKey(nAppID); }
	bool GetCurrentBetaName(char * pchName, int cchNameBufferSize) override { return Upstream->GetCurrentBetaName(pchName, cchNameBufferSize); }
	bool MarkContentCorrupt(bool bMissingFilesOnly) override { return Upstream->MarkContentCorrupt(bMissingFilesOnly); }
	uint32 GetInstalledDepots(AppId_t appID, DepotId_t * pvecDepots, uint32 cMaxDepots) override { return Upstream->GetInstalledDepots(appID, pvecDepots, cMaxDepots); }
	uint32 GetAppInstallDir(AppId_t appID, char * pchFolder, uint32 cchFolderBufferSize) override { return Upstream->GetAppInstallDir(appID, pchFolder, cchFolderBufferSize); }
	bool BIsAppInstalled(AppId_t appID) override { return Upstream->BIsAppInstalled(appID); }
	CSteamID GetAppOwner() override { return Upstream->GetAppOwner(); }
	char const * GetLaunchQueryParam(char const * pchKey) override { return Upstream->GetLaunchQueryParam(pchKey); }
	bool GetDlcDownloadProgress(AppId_t nAppID, uint64 * punBytesDownloaded, uint64 * punBytesTotal) override { return Upstream->GetDlcDownloadProgress(nAppID, punBytesDownloaded, punBytesTotal); }
	int GetAppBuildId() override { return Upstream->GetAppBuildId(); }
	void RequestAllProofOfPurchaseKeys() override { return Upstream->RequestAllProofOfPurchaseKeys(); }
	SteamAPICall_t GetFileDetails(char const * pszFileName) override { return Upstream->GetFileDetails(pszFileName); }
	int GetLaunchCommandLine(char * pszCommandLine, int cubCommandLine) override { return Upstream->GetLaunchCommandLine(pszCommandLine, cubCommandLine); }
	bool BIsSubscribedFromFamilySharing() override { return Upstream->BIsSubscribedFromFamilySharing(); }
	bool BIsTimedTrial(uint32 * punSecondsAllowed, uint32 * punSecondsPlayed) override { return Upstream->BIsTimedTrial(punSecondsAllowed, punSecondsPlayed); }
	bool SetDlcContext(AppId_t nAppID) override { return Upstream->SetDlcContext(nAppID); }
};

class ProxySteamClient007: public ISteamClient007 {
protected:
	ISteamClient007* Upstream;

public:
	ProxySteamClient007(ISteamClient007* Upstream):
		Upstream{ Upstream }
	{}

	HSteamPipe CreateSteamPipe() override { return Upstream->CreateSteamPipe(); }
	bool BReleaseSteamPipe(HSteamPipe hSteamPipe) override { return Upstream->BReleaseSteamPipe(hSteamPipe); }
	HSteamUser ConnectToGlobalUser(HSteamPipe hSteamPipe) override { return Upstream->ConnectToGlobalUser(hSteamPipe); }
	HSteamUser CreateLocalUser(HSteamPipe * phSteamPipe) override { return Upstream->CreateLocalUser(phSteamPipe); }
	void ReleaseUser(HSteamPipe hSteamPipe, HSteamUser hUser) override { return Upstream->ReleaseUser(hSteamPipe, hUser); }
	ISteamUser * GetISteamUser(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUser(hSteamUser, hSteamPipe, pchVersion); }
	ISteamGameServer * GetISteamGameServer(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGameServer(hSteamUser, hSteamPipe, pchVersion); }
	void SetLocalIPBinding(uint32 unIP, uint16 usPort) override { return Upstream->SetLocalIPBinding(unIP, usPort); }
	ISteamFriends * GetISteamFriends(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamFriends(hSteamUser, hSteamPipe, pchVersion); }
	ISteamUtils * GetISteamUtils(HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUtils(hSteamPipe, pchVersion); }
	ISteamMatchmaking * GetISteamMatchmaking(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMatchmaking(hSteamUser, hSteamPipe, pchVersion); }
	ISteamContentServer * GetISteamContentServer(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamContentServer(hSteamUser, hSteamPipe, pchVersion); }
	ISteamMasterServerUpdater * GetISteamMasterServerUpdater(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMasterServerUpdater(hSteamUser, hSteamPipe, pchVersion); }
	ISteamMatchmakingServers * GetISteamMatchmakingServers(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMatchmakingServers(hSteamUser, hSteamPipe, pchVersion); }
	void * GetISteamGenericInterface(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGenericInterface(hSteamUser, hSteamPipe, pchVersion); }
	void RunFrame() override { return Upstream->RunFrame(); }
	uint32 GetIPCCallCount() override { return Upstream->GetIPCCallCount(); }
	ISteamUserStats * GetISteamUserStats(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUserStats(hSteamUser, hSteamPipe, pchVersion); }
	ISteamApps * GetISteamApps(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamApps(hSteamUser, hSteamPipe, pchVersion); }
	ISteamNetworking * GetISteamNetworking(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamNetworking(hSteamUser, hSteamPipe, pchVersion); }
	void SetWarningMessageHook(SteamAPIWarningMessageHook_t pFunction) override { return Upstream->SetWarningMessageHook(pFunction); }
};

class ProxySteamClient008: public ISteamClient008 {
protected:
	ISteamClient008* Upstream;

public:
	ProxySteamClient008(ISteamClient008* Upstream):
		Upstream{ Upstream }
	{}

	HSteamPipe CreateSteamPipe() override { return Upstream->CreateSteamPipe(); }
	bool BReleaseSteamPipe(HSteamPipe hSteamPipe) override { return Upstream->BReleaseSteamPipe(hSteamPipe); }
	HSteamUser ConnectToGlobalUser(HSteamPipe hSteamPipe) override { return Upstream->ConnectToGlobalUser(hSteamPipe); }
	HSteamUser CreateLocalUser(HSteamPipe * phSteamPipe, EAccountType eAccountType) override { return Upstream->CreateLocalUser(phSteamPipe, eAccountType); }
	void ReleaseUser(HSteamPipe hSteamPipe, HSteamUser hUser) override { return Upstream->ReleaseUser(hSteamPipe, hUser); }
	ISteamUser * GetISteamUser(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUser(hSteamUser, hSteamPipe, pchVersion); }
	ISteamGameServer * GetISteamGameServer(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGameServer(hSteamUser, hSteamPipe, pchVersion); }
	void SetLocalIPBinding(uint32 unIP, uint16 usPort) override { return Upstream->SetLocalIPBinding(unIP, usPort); }
	ISteamFriends * GetISteamFriends(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamFriends(hSteamUser, hSteamPipe, pchVersion); }
	ISteamUtils * GetISteamUtils(HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUtils(hSteamPipe, pchVersion); }
	ISteamMatchmaking * GetISteamMatchmaking(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMatchmaking(hSteamUser, hSteamPipe, pchVersion); }
	ISteamMasterServerUpdater * GetISteamMasterServerUpdater(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMasterServerUpdater(hSteamUser, hSteamPipe, pchVersion); }
	ISteamMatchmakingServers * GetISteamMatchmakingServers(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMatchmakingServers(hSteamUser, hSteamPipe, pchVersion); }
	void * GetISteamGenericInterface(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGenericInterface(hSteamUser, hSteamPipe, pchVersion); }
	ISteamUserStats * GetISteamUserStats(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUserStats(hSteamUser, hSteamPipe, pchVersion); }
	ISteamApps * GetISteamApps(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamApps(hSteamUser, hSteamPipe, pchVersion); }
	ISteamNetworking * GetISteamNetworking(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamNetworking(hSteamUser, hSteamPipe, pchVersion); }
	ISteamRemoteStorage * GetISteamRemoteStorage(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamRemoteStorage(hSteamuser, hSteamPipe, pchVersion); }
	void RunFrame() override { return Upstream->RunFrame(); }
	uint32 GetIPCCallCount() override { return Upstream->GetIPCCallCount(); }
	void SetWarningMessageHook(SteamAPIWarningMessageHook_t pFunction) override { return Upstream->SetWarningMessageHook(pFunction); }
};

class ProxySteamClient009: public ISteamClient009 {
protected:
	ISteamClient009* Upstream;

public:
	ProxySteamClient009(ISteamClient009* Upstream):
		Upstream{ Upstream }
	{}

	HSteamPipe CreateSteamPipe() override { return Upstream->CreateSteamPipe(); }
	bool BReleaseSteamPipe(HSteamPipe hSteamPipe) override { return Upstream->BReleaseSteamPipe(hSteamPipe); }
	HSteamUser ConnectToGlobalUser(HSteamPipe hSteamPipe) override { return Upstream->ConnectToGlobalUser(hSteamPipe); }
	HSteamUser CreateLocalUser(HSteamPipe * phSteamPipe, EAccountType eAccountType) override { return Upstream->CreateLocalUser(phSteamPipe, eAccountType); }
	void ReleaseUser(HSteamPipe hSteamPipe, HSteamUser hUser) override { return Upstream->ReleaseUser(hSteamPipe, hUser); }
	ISteamUser * GetISteamUser(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUser(hSteamUser, hSteamPipe, pchVersion); }
	ISteamGameServer * GetISteamGameServer(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGameServer(hSteamUser, hSteamPipe, pchVersion); }
	void SetLocalIPBinding(uint32 unIP, uint16 usPort) override { return Upstream->SetLocalIPBinding(unIP, usPort); }
	ISteamFriends * GetISteamFriends(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamFriends(hSteamUser, hSteamPipe, pchVersion); }
	ISteamUtils * GetISteamUtils(HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUtils(hSteamPipe, pchVersion); }
	ISteamMatchmaking * GetISteamMatchmaking(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMatchmaking(hSteamUser, hSteamPipe, pchVersion); }
	ISteamMasterServerUpdater * GetISteamMasterServerUpdater(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMasterServerUpdater(hSteamUser, hSteamPipe, pchVersion); }
	ISteamMatchmakingServers * GetISteamMatchmakingServers(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMatchmakingServers(hSteamUser, hSteamPipe, pchVersion); }
	void * GetISteamGenericInterface(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGenericInterface(hSteamUser, hSteamPipe, pchVersion); }
	ISteamUserStats * GetISteamUserStats(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUserStats(hSteamUser, hSteamPipe, pchVersion); }
	ISteamGameServerStats * GetISteamGameServerStats(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGameServerStats(hSteamuser, hSteamPipe, pchVersion); }
	ISteamApps * GetISteamApps(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamApps(hSteamUser, hSteamPipe, pchVersion); }
	ISteamNetworking * GetISteamNetworking(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamNetworking(hSteamUser, hSteamPipe, pchVersion); }
	ISteamRemoteStorage * GetISteamRemoteStorage(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamRemoteStorage(hSteamuser, hSteamPipe, pchVersion); }
	void RunFrame() override { return Upstream->RunFrame(); }
	uint32 GetIPCCallCount() override { return Upstream->GetIPCCallCount(); }
	void SetWarningMessageHook(SteamAPIWarningMessageHook_t pFunction) override { return Upstream->SetWarningMessageHook(pFunction); }
};

class ProxySteamClient010: public ISteamClient010 {
protected:
	ISteamClient010* Upstream;

public:
	ProxySteamClient010(ISteamClient010* Upstream):
		Upstream{ Upstream }
	{}

	HSteamPipe CreateSteamPipe() override { return Upstream->CreateSteamPipe(); }
	bool BReleaseSteamPipe(HSteamPipe hSteamPipe) override { return Upstream->BReleaseSteamPipe(hSteamPipe); }
	HSteamUser ConnectToGlobalUser(HSteamPipe hSteamPipe) override { return Upstream->ConnectToGlobalUser(hSteamPipe); }
	HSteamUser CreateLocalUser(HSteamPipe * phSteamPipe, EAccountType eAccountType) override { return Upstream->CreateLocalUser(phSteamPipe, eAccountType); }
	void ReleaseUser(HSteamPipe hSteamPipe, HSteamUser hUser) override { return Upstream->ReleaseUser(hSteamPipe, hUser); }
	ISteamUser * GetISteamUser(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUser(hSteamUser, hSteamPipe, pchVersion); }
	ISteamGameServer * GetISteamGameServer(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGameServer(hSteamUser, hSteamPipe, pchVersion); }
	void SetLocalIPBinding(uint32 unIP, uint16 usPort) override { return Upstream->SetLocalIPBinding(unIP, usPort); }
	ISteamFriends * GetISteamFriends(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamFriends(hSteamUser, hSteamPipe, pchVersion); }
	ISteamUtils * GetISteamUtils(HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUtils(hSteamPipe, pchVersion); }
	ISteamMatchmaking * GetISteamMatchmaking(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMatchmaking(hSteamUser, hSteamPipe, pchVersion); }
	ISteamMasterServerUpdater * GetISteamMasterServerUpdater(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMasterServerUpdater(hSteamUser, hSteamPipe, pchVersion); }
	ISteamMatchmakingServers * GetISteamMatchmakingServers(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMatchmakingServers(hSteamUser, hSteamPipe, pchVersion); }
	void * GetISteamGenericInterface(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGenericInterface(hSteamUser, hSteamPipe, pchVersion); }
	ISteamUserStats * GetISteamUserStats(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUserStats(hSteamUser, hSteamPipe, pchVersion); }
	ISteamGameServerStats * GetISteamGameServerStats(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGameServerStats(hSteamuser, hSteamPipe, pchVersion); }
	ISteamApps * GetISteamApps(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamApps(hSteamUser, hSteamPipe, pchVersion); }
	ISteamNetworking * GetISteamNetworking(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamNetworking(hSteamUser, hSteamPipe, pchVersion); }
	ISteamRemoteStorage * GetISteamRemoteStorage(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamRemoteStorage(hSteamuser, hSteamPipe, pchVersion); }
	void RunFrame() override { return Upstream->RunFrame(); }
	uint32 GetIPCCallCount() override { return Upstream->GetIPCCallCount(); }
	void SetWarningMessageHook(SteamAPIWarningMessageHook_t pFunction) override { return Upstream->SetWarningMessageHook(pFunction); }
	bool BShutdownIfAllPipesClosed() override { return Upstream->BShutdownIfAllPipesClosed(); }
	ISteamHTTP * GetISteamHTTP(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamHTTP(hSteamuser, hSteamPipe, pchVersion); }
};

class ProxySteamClient011: public ISteamClient011 {
protected:
	ISteamClient011* Upstream;

public:
	ProxySteamClient011(ISteamClient011* Upstream):
		Upstream{ Upstream }
	{}

	HSteamPipe CreateSteamPipe() override { return Upstream->CreateSteamPipe(); }
	bool BReleaseSteamPipe(HSteamPipe hSteamPipe) override { return Upstream->BReleaseSteamPipe(hSteamPipe); }
	HSteamUser ConnectToGlobalUser(HSteamPipe hSteamPipe) override { return Upstream->ConnectToGlobalUser(hSteamPipe); }
	HSteamUser CreateLocalUser(HSteamPipe * phSteamPipe, EAccountType eAccountType) override { return Upstream->CreateLocalUser(phSteamPipe, eAccountType); }
	void ReleaseUser(HSteamPipe hSteamPipe, HSteamUser hUser) override { return Upstream->ReleaseUser(hSteamPipe, hUser); }
	ISteamUser * GetISteamUser(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUser(hSteamUser, hSteamPipe, pchVersion); }
	ISteamGameServer * GetISteamGameServer(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGameServer(hSteamUser, hSteamPipe, pchVersion); }
	void SetLocalIPBinding(uint32 unIP, uint16 usPort) override { return Upstream->SetLocalIPBinding(unIP, usPort); }
	ISteamFriends * GetISteamFriends(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamFriends(hSteamUser, hSteamPipe, pchVersion); }
	ISteamUtils * GetISteamUtils(HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUtils(hSteamPipe, pchVersion); }
	ISteamMatchmaking * GetISteamMatchmaking(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMatchmaking(hSteamUser, hSteamPipe, pchVersion); }
	ISteamMasterServerUpdater * GetISteamMasterServerUpdater(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMasterServerUpdater(hSteamUser, hSteamPipe, pchVersion); }
	ISteamMatchmakingServers * GetISteamMatchmakingServers(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMatchmakingServers(hSteamUser, hSteamPipe, pchVersion); }
	void * GetISteamGenericInterface(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGenericInterface(hSteamUser, hSteamPipe, pchVersion); }
	ISteamUserStats * GetISteamUserStats(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUserStats(hSteamUser, hSteamPipe, pchVersion); }
	ISteamGameServerStats * GetISteamGameServerStats(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGameServerStats(hSteamuser, hSteamPipe, pchVersion); }
	ISteamApps * GetISteamApps(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamApps(hSteamUser, hSteamPipe, pchVersion); }
	ISteamNetworking * GetISteamNetworking(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamNetworking(hSteamUser, hSteamPipe, pchVersion); }
	ISteamRemoteStorage * GetISteamRemoteStorage(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamRemoteStorage(hSteamuser, hSteamPipe, pchVersion); }
	ISteamScreenshots * GetISteamScreenshots(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamScreenshots(hSteamuser, hSteamPipe, pchVersion); }
	void RunFrame() override { return Upstream->RunFrame(); }
	uint32 GetIPCCallCount() override { return Upstream->GetIPCCallCount(); }
	void SetWarningMessageHook(SteamAPIWarningMessageHook_t pFunction) override { return Upstream->SetWarningMessageHook(pFunction); }
	bool BShutdownIfAllPipesClosed() override { return Upstream->BShutdownIfAllPipesClosed(); }
	ISteamHTTP * GetISteamHTTP(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamHTTP(hSteamuser, hSteamPipe, pchVersion); }
};

class ProxySteamClient012: public ISteamClient012 {
protected:
	ISteamClient012* Upstream;

public:
	ProxySteamClient012(ISteamClient012* Upstream):
		Upstream{ Upstream }
	{}

	HSteamPipe CreateSteamPipe() override { return Upstream->CreateSteamPipe(); }
	bool BReleaseSteamPipe(HSteamPipe hSteamPipe) override { return Upstream->BReleaseSteamPipe(hSteamPipe); }
	HSteamUser ConnectToGlobalUser(HSteamPipe hSteamPipe) override { return Upstream->ConnectToGlobalUser(hSteamPipe); }
	HSteamUser CreateLocalUser(HSteamPipe * phSteamPipe, EAccountType eAccountType) override { return Upstream->CreateLocalUser(phSteamPipe, eAccountType); }
	void ReleaseUser(HSteamPipe hSteamPipe, HSteamUser hUser) override { return Upstream->ReleaseUser(hSteamPipe, hUser); }
	ISteamUser * GetISteamUser(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUser(hSteamUser, hSteamPipe, pchVersion); }
	ISteamGameServer * GetISteamGameServer(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGameServer(hSteamUser, hSteamPipe, pchVersion); }
	void SetLocalIPBinding(uint32 unIP, uint16 usPort) override { return Upstream->SetLocalIPBinding(unIP, usPort); }
	ISteamFriends * GetISteamFriends(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamFriends(hSteamUser, hSteamPipe, pchVersion); }
	ISteamUtils * GetISteamUtils(HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUtils(hSteamPipe, pchVersion); }
	ISteamMatchmaking * GetISteamMatchmaking(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMatchmaking(hSteamUser, hSteamPipe, pchVersion); }
	ISteamMatchmakingServers * GetISteamMatchmakingServers(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMatchmakingServers(hSteamUser, hSteamPipe, pchVersion); }
	void * GetISteamGenericInterface(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGenericInterface(hSteamUser, hSteamPipe, pchVersion); }
	ISteamUserStats * GetISteamUserStats(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUserStats(hSteamUser, hSteamPipe, pchVersion); }
	ISteamGameServerStats * GetISteamGameServerStats(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGameServerStats(hSteamuser, hSteamPipe, pchVersion); }
	ISteamApps * GetISteamApps(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamApps(hSteamUser, hSteamPipe, pchVersion); }
	ISteamNetworking * GetISteamNetworking(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamNetworking(hSteamUser, hSteamPipe, pchVersion); }
	ISteamRemoteStorage * GetISteamRemoteStorage(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamRemoteStorage(hSteamuser, hSteamPipe, pchVersion); }
	ISteamScreenshots * GetISteamScreenshots(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamScreenshots(hSteamuser, hSteamPipe, pchVersion); }
	void RunFrame() override { return Upstream->RunFrame(); }
	uint32 GetIPCCallCount() override { return Upstream->GetIPCCallCount(); }
	void SetWarningMessageHook(SteamAPIWarningMessageHook_t pFunction) override { return Upstream->SetWarningMessageHook(pFunction); }
	bool BShutdownIfAllPipesClosed() override { return Upstream->BShutdownIfAllPipesClosed(); }
	ISteamHTTP * GetISteamHTTP(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamHTTP(hSteamuser, hSteamPipe, pchVersion); }
	ISteamUnifiedMessages * GetISteamUnifiedMessages(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUnifiedMessages(hSteamuser, hSteamPipe, pchVersion); }
	ISteamController * GetISteamController(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamController(hSteamUser, hSteamPipe, pchVersion); }
	ISteamUGC * GetISteamUGC(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUGC(hSteamUser, hSteamPipe, pchVersion); }
};

class ProxySteamClient013: public ISteamClient013 {
protected:
	ISteamClient013* Upstream;

public:
	ProxySteamClient013(ISteamClient013* Upstream):
		Upstream{ Upstream }
	{}

	HSteamPipe CreateSteamPipe() override { return Upstream->CreateSteamPipe(); }
	bool BReleaseSteamPipe(HSteamPipe hSteamPipe) override { return Upstream->BReleaseSteamPipe(hSteamPipe); }
	HSteamUser ConnectToGlobalUser(HSteamPipe hSteamPipe) override { return Upstream->ConnectToGlobalUser(hSteamPipe); }
	HSteamUser CreateLocalUser(HSteamPipe * phSteamPipe, EAccountType eAccountType) override { return Upstream->CreateLocalUser(phSteamPipe, eAccountType); }
	void ReleaseUser(HSteamPipe hSteamPipe, HSteamUser hUser) override { return Upstream->ReleaseUser(hSteamPipe, hUser); }
	ISteamUser * GetISteamUser(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUser(hSteamUser, hSteamPipe, pchVersion); }
	ISteamGameServer * GetISteamGameServer(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGameServer(hSteamUser, hSteamPipe, pchVersion); }
	void SetLocalIPBinding(uint32 unIP, uint16 usPort) override { return Upstream->SetLocalIPBinding(unIP, usPort); }
	ISteamFriends * GetISteamFriends(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamFriends(hSteamUser, hSteamPipe, pchVersion); }
	ISteamUtils * GetISteamUtils(HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUtils(hSteamPipe, pchVersion); }
	ISteamMatchmaking * GetISteamMatchmaking(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMatchmaking(hSteamUser, hSteamPipe, pchVersion); }
	ISteamMatchmakingServers * GetISteamMatchmakingServers(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMatchmakingServers(hSteamUser, hSteamPipe, pchVersion); }
	void * GetISteamGenericInterface(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGenericInterface(hSteamUser, hSteamPipe, pchVersion); }
	ISteamUserStats * GetISteamUserStats(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUserStats(hSteamUser, hSteamPipe, pchVersion); }
	ISteamGameServerStats * GetISteamGameServerStats(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGameServerStats(hSteamuser, hSteamPipe, pchVersion); }
	ISteamApps * GetISteamApps(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamApps(hSteamUser, hSteamPipe, pchVersion); }
	ISteamNetworking * GetISteamNetworking(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamNetworking(hSteamUser, hSteamPipe, pchVersion); }
	ISteamRemoteStorage * GetISteamRemoteStorage(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamRemoteStorage(hSteamuser, hSteamPipe, pchVersion); }
	ISteamScreenshots * GetISteamScreenshots(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamScreenshots(hSteamuser, hSteamPipe, pchVersion); }
	void RunFrame() override { return Upstream->RunFrame(); }
	uint32 GetIPCCallCount() override { return Upstream->GetIPCCallCount(); }
	void SetWarningMessageHook(SteamAPIWarningMessageHook_t pFunction) override { return Upstream->SetWarningMessageHook(pFunction); }
	bool BShutdownIfAllPipesClosed() override { return Upstream->BShutdownIfAllPipesClosed(); }
	ISteamHTTP * GetISteamHTTP(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamHTTP(hSteamuser, hSteamPipe, pchVersion); }
	ISteamUnifiedMessages * GetISteamUnifiedMessages(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUnifiedMessages(hSteamuser, hSteamPipe, pchVersion); }
	ISteamController * GetISteamController(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamController(hSteamUser, hSteamPipe, pchVersion); }
	ISteamUGC * GetISteamUGC(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUGC(hSteamUser, hSteamPipe, pchVersion); }
	ISteamInventory * GetISteamInventory(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamInventory(hSteamUser, hSteamPipe, pchVersion); }
	ISteamVideo * GetISteamVideo(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamVideo(hSteamUser, hSteamPipe, pchVersion); }
	ISteamAppList * GetISteamAppList(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamAppList(hSteamUser, hSteamPipe, pchVersion); }
};

class ProxySteamClient014: public ISteamClient014 {
protected:
	ISteamClient014* Upstream;

public:
	ProxySteamClient014(ISteamClient014* Upstream):
		Upstream{ Upstream }
	{}

	HSteamPipe CreateSteamPipe() override { return Upstream->CreateSteamPipe(); }
	bool BReleaseSteamPipe(HSteamPipe hSteamPipe) override { return Upstream->BReleaseSteamPipe(hSteamPipe); }
	HSteamUser ConnectToGlobalUser(HSteamPipe hSteamPipe) override { return Upstream->ConnectToGlobalUser(hSteamPipe); }
	HSteamUser CreateLocalUser(HSteamPipe * phSteamPipe, EAccountType eAccountType) override { return Upstream->CreateLocalUser(phSteamPipe, eAccountType); }
	void ReleaseUser(HSteamPipe hSteamPipe, HSteamUser hUser) override { return Upstream->ReleaseUser(hSteamPipe, hUser); }
	ISteamUser * GetISteamUser(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUser(hSteamUser, hSteamPipe, pchVersion); }
	ISteamGameServer * GetISteamGameServer(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGameServer(hSteamUser, hSteamPipe, pchVersion); }
	void SetLocalIPBinding(uint32 unIP, uint16 usPort) override { return Upstream->SetLocalIPBinding(unIP, usPort); }
	ISteamFriends * GetISteamFriends(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamFriends(hSteamUser, hSteamPipe, pchVersion); }
	ISteamUtils * GetISteamUtils(HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUtils(hSteamPipe, pchVersion); }
	ISteamMatchmaking * GetISteamMatchmaking(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMatchmaking(hSteamUser, hSteamPipe, pchVersion); }
	ISteamMatchmakingServers * GetISteamMatchmakingServers(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMatchmakingServers(hSteamUser, hSteamPipe, pchVersion); }
	void * GetISteamGenericInterface(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGenericInterface(hSteamUser, hSteamPipe, pchVersion); }
	ISteamUserStats * GetISteamUserStats(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUserStats(hSteamUser, hSteamPipe, pchVersion); }
	ISteamGameServerStats * GetISteamGameServerStats(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGameServerStats(hSteamuser, hSteamPipe, pchVersion); }
	ISteamApps * GetISteamApps(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamApps(hSteamUser, hSteamPipe, pchVersion); }
	ISteamNetworking * GetISteamNetworking(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamNetworking(hSteamUser, hSteamPipe, pchVersion); }
	ISteamRemoteStorage * GetISteamRemoteStorage(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamRemoteStorage(hSteamuser, hSteamPipe, pchVersion); }
	ISteamScreenshots * GetISteamScreenshots(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamScreenshots(hSteamuser, hSteamPipe, pchVersion); }
	void RunFrame() override { return Upstream->RunFrame(); }
	uint32 GetIPCCallCount() override { return Upstream->GetIPCCallCount(); }
	void SetWarningMessageHook(SteamAPIWarningMessageHook_t pFunction) override { return Upstream->SetWarningMessageHook(pFunction); }
	bool BShutdownIfAllPipesClosed() override { return Upstream->BShutdownIfAllPipesClosed(); }
	ISteamHTTP * GetISteamHTTP(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamHTTP(hSteamuser, hSteamPipe, pchVersion); }
	ISteamUnifiedMessages * GetISteamUnifiedMessages(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUnifiedMessages(hSteamuser, hSteamPipe, pchVersion); }
	ISteamController * GetISteamController(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamController(hSteamUser, hSteamPipe, pchVersion); }
	ISteamUGC * GetISteamUGC(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUGC(hSteamUser, hSteamPipe, pchVersion); }
	ISteamAppList * GetISteamAppList(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamAppList(hSteamUser, hSteamPipe, pchVersion); }
	ISteamMusic * GetISteamMusic(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMusic(hSteamuser, hSteamPipe, pchVersion); }
};

class ProxySteamClient015: public ISteamClient015 {
protected:
	ISteamClient015* Upstream;

public:
	ProxySteamClient015(ISteamClient015* Upstream):
		Upstream{ Upstream }
	{}

	HSteamPipe CreateSteamPipe() override { return Upstream->CreateSteamPipe(); }
	bool BReleaseSteamPipe(HSteamPipe hSteamPipe) override { return Upstream->BReleaseSteamPipe(hSteamPipe); }
	HSteamUser ConnectToGlobalUser(HSteamPipe hSteamPipe) override { return Upstream->ConnectToGlobalUser(hSteamPipe); }
	HSteamUser CreateLocalUser(HSteamPipe * phSteamPipe, EAccountType eAccountType) override { return Upstream->CreateLocalUser(phSteamPipe, eAccountType); }
	void ReleaseUser(HSteamPipe hSteamPipe, HSteamUser hUser) override { return Upstream->ReleaseUser(hSteamPipe, hUser); }
	ISteamUser * GetISteamUser(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUser(hSteamUser, hSteamPipe, pchVersion); }
	ISteamGameServer * GetISteamGameServer(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGameServer(hSteamUser, hSteamPipe, pchVersion); }
	void SetLocalIPBinding(uint32 unIP, uint16 usPort) override { return Upstream->SetLocalIPBinding(unIP, usPort); }
	ISteamFriends * GetISteamFriends(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamFriends(hSteamUser, hSteamPipe, pchVersion); }
	ISteamUtils * GetISteamUtils(HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUtils(hSteamPipe, pchVersion); }
	ISteamMatchmaking * GetISteamMatchmaking(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMatchmaking(hSteamUser, hSteamPipe, pchVersion); }
	ISteamMatchmakingServers * GetISteamMatchmakingServers(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMatchmakingServers(hSteamUser, hSteamPipe, pchVersion); }
	void * GetISteamGenericInterface(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGenericInterface(hSteamUser, hSteamPipe, pchVersion); }
	ISteamUserStats * GetISteamUserStats(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUserStats(hSteamUser, hSteamPipe, pchVersion); }
	ISteamGameServerStats * GetISteamGameServerStats(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGameServerStats(hSteamuser, hSteamPipe, pchVersion); }
	ISteamApps * GetISteamApps(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamApps(hSteamUser, hSteamPipe, pchVersion); }
	ISteamNetworking * GetISteamNetworking(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamNetworking(hSteamUser, hSteamPipe, pchVersion); }
	ISteamRemoteStorage * GetISteamRemoteStorage(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamRemoteStorage(hSteamuser, hSteamPipe, pchVersion); }
	ISteamScreenshots * GetISteamScreenshots(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamScreenshots(hSteamuser, hSteamPipe, pchVersion); }
	void RunFrame() override { return Upstream->RunFrame(); }
	uint32 GetIPCCallCount() override { return Upstream->GetIPCCallCount(); }
	void SetWarningMessageHook(SteamAPIWarningMessageHook_t pFunction) override { return Upstream->SetWarningMessageHook(pFunction); }
	bool BShutdownIfAllPipesClosed() override { return Upstream->BShutdownIfAllPipesClosed(); }
	ISteamHTTP * GetISteamHTTP(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamHTTP(hSteamuser, hSteamPipe, pchVersion); }
	ISteamUnifiedMessages * GetISteamUnifiedMessages(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUnifiedMessages(hSteamuser, hSteamPipe, pchVersion); }
	ISteamController * GetISteamController(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamController(hSteamUser, hSteamPipe, pchVersion); }
	ISteamUGC * GetISteamUGC(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUGC(hSteamUser, hSteamPipe, pchVersion); }
	ISteamAppList * GetISteamAppList(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamAppList(hSteamUser, hSteamPipe, pchVersion); }
	ISteamMusic * GetISteamMusic(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMusic(hSteamuser, hSteamPipe, pchVersion); }
	ISteamMusicRemote * GetISteamMusicRemote(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMusicRemote(hSteamuser, hSteamPipe, pchVersion); }
};

class ProxySteamClient016: public ISteamClient016 {
protected:
	ISteamClient016* Upstream;

public:
	ProxySteamClient016(ISteamClient016* Upstream):
		Upstream{ Upstream }
	{}

	HSteamPipe CreateSteamPipe() override { return Upstream->CreateSteamPipe(); }
	bool BReleaseSteamPipe(HSteamPipe hSteamPipe) override { return Upstream->BReleaseSteamPipe(hSteamPipe); }
	HSteamUser ConnectToGlobalUser(HSteamPipe hSteamPipe) override { return Upstream->ConnectToGlobalUser(hSteamPipe); }
	HSteamUser CreateLocalUser(HSteamPipe * phSteamPipe, EAccountType eAccountType) override { return Upstream->CreateLocalUser(phSteamPipe, eAccountType); }
	void ReleaseUser(HSteamPipe hSteamPipe, HSteamUser hUser) override { return Upstream->ReleaseUser(hSteamPipe, hUser); }
	ISteamUser * GetISteamUser(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUser(hSteamUser, hSteamPipe, pchVersion); }
	ISteamGameServer * GetISteamGameServer(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGameServer(hSteamUser, hSteamPipe, pchVersion); }
	void SetLocalIPBinding(uint32 unIP, uint16 usPort) override { return Upstream->SetLocalIPBinding(unIP, usPort); }
	ISteamFriends * GetISteamFriends(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamFriends(hSteamUser, hSteamPipe, pchVersion); }
	ISteamUtils * GetISteamUtils(HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUtils(hSteamPipe, pchVersion); }
	ISteamMatchmaking * GetISteamMatchmaking(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMatchmaking(hSteamUser, hSteamPipe, pchVersion); }
	ISteamMatchmakingServers * GetISteamMatchmakingServers(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMatchmakingServers(hSteamUser, hSteamPipe, pchVersion); }
	void * GetISteamGenericInterface(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGenericInterface(hSteamUser, hSteamPipe, pchVersion); }
	ISteamUserStats * GetISteamUserStats(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUserStats(hSteamUser, hSteamPipe, pchVersion); }
	ISteamGameServerStats * GetISteamGameServerStats(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGameServerStats(hSteamuser, hSteamPipe, pchVersion); }
	ISteamApps * GetISteamApps(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamApps(hSteamUser, hSteamPipe, pchVersion); }
	ISteamNetworking * GetISteamNetworking(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamNetworking(hSteamUser, hSteamPipe, pchVersion); }
	ISteamRemoteStorage * GetISteamRemoteStorage(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamRemoteStorage(hSteamuser, hSteamPipe, pchVersion); }
	ISteamScreenshots * GetISteamScreenshots(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamScreenshots(hSteamuser, hSteamPipe, pchVersion); }
	void RunFrame() override { return Upstream->RunFrame(); }
	uint32 GetIPCCallCount() override { return Upstream->GetIPCCallCount(); }
	void SetWarningMessageHook(SteamAPIWarningMessageHook_t pFunction) override { return Upstream->SetWarningMessageHook(pFunction); }
	bool BShutdownIfAllPipesClosed() override { return Upstream->BShutdownIfAllPipesClosed(); }
	ISteamHTTP * GetISteamHTTP(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamHTTP(hSteamuser, hSteamPipe, pchVersion); }
	ISteamUnifiedMessages * GetISteamUnifiedMessages(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUnifiedMessages(hSteamuser, hSteamPipe, pchVersion); }
	ISteamController * GetISteamController(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamController(hSteamUser, hSteamPipe, pchVersion); }
	ISteamUGC * GetISteamUGC(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUGC(hSteamUser, hSteamPipe, pchVersion); }
	ISteamAppList * GetISteamAppList(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamAppList(hSteamUser, hSteamPipe, pchVersion); }
	ISteamMusic * GetISteamMusic(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMusic(hSteamuser, hSteamPipe, pchVersion); }
	ISteamMusicRemote * GetISteamMusicRemote(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMusicRemote(hSteamuser, hSteamPipe, pchVersion); }
	ISteamHTMLSurface * GetISteamHTMLSurface(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamHTMLSurface(hSteamuser, hSteamPipe, pchVersion); }
	void Set_SteamAPI_CPostAPIResultInProcess(SteamAPI_PostAPIResultInProcess_t func) override { return Upstream->Set_SteamAPI_CPostAPIResultInProcess(func); }
	void Remove_SteamAPI_CPostAPIResultInProcess(SteamAPI_PostAPIResultInProcess_t func) override { return Upstream->Remove_SteamAPI_CPostAPIResultInProcess(func); }
	void Set_SteamAPI_CCheckCallbackRegisteredInProcess(SteamAPI_CheckCallbackRegistered_t func) override { return Upstream->Set_SteamAPI_CCheckCallbackRegisteredInProcess(func); }
};

class ProxySteamClient017: public ISteamClient017 {
protected:
	ISteamClient017* Upstream;

public:
	ProxySteamClient017(ISteamClient017* Upstream):
		Upstream{ Upstream }
	{}

	HSteamPipe CreateSteamPipe() override { return Upstream->CreateSteamPipe(); }
	bool BReleaseSteamPipe(HSteamPipe hSteamPipe) override { return Upstream->BReleaseSteamPipe(hSteamPipe); }
	HSteamUser ConnectToGlobalUser(HSteamPipe hSteamPipe) override { return Upstream->ConnectToGlobalUser(hSteamPipe); }
	HSteamUser CreateLocalUser(HSteamPipe * phSteamPipe, EAccountType eAccountType) override { return Upstream->CreateLocalUser(phSteamPipe, eAccountType); }
	void ReleaseUser(HSteamPipe hSteamPipe, HSteamUser hUser) override { return Upstream->ReleaseUser(hSteamPipe, hUser); }
	ISteamUser * GetISteamUser(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUser(hSteamUser, hSteamPipe, pchVersion); }
	ISteamGameServer * GetISteamGameServer(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGameServer(hSteamUser, hSteamPipe, pchVersion); }
	void SetLocalIPBinding(uint32 unIP, uint16 usPort) override { return Upstream->SetLocalIPBinding(unIP, usPort); }
	ISteamFriends * GetISteamFriends(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamFriends(hSteamUser, hSteamPipe, pchVersion); }
	ISteamUtils * GetISteamUtils(HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUtils(hSteamPipe, pchVersion); }
	ISteamMatchmaking * GetISteamMatchmaking(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMatchmaking(hSteamUser, hSteamPipe, pchVersion); }
	ISteamMatchmakingServers * GetISteamMatchmakingServers(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMatchmakingServers(hSteamUser, hSteamPipe, pchVersion); }
	void * GetISteamGenericInterface(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGenericInterface(hSteamUser, hSteamPipe, pchVersion); }
	ISteamUserStats * GetISteamUserStats(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUserStats(hSteamUser, hSteamPipe, pchVersion); }
	ISteamGameServerStats * GetISteamGameServerStats(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGameServerStats(hSteamuser, hSteamPipe, pchVersion); }
	ISteamApps * GetISteamApps(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamApps(hSteamUser, hSteamPipe, pchVersion); }
	ISteamNetworking * GetISteamNetworking(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamNetworking(hSteamUser, hSteamPipe, pchVersion); }
	ISteamRemoteStorage * GetISteamRemoteStorage(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamRemoteStorage(hSteamuser, hSteamPipe, pchVersion); }
	ISteamScreenshots * GetISteamScreenshots(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamScreenshots(hSteamuser, hSteamPipe, pchVersion); }
	void RunFrame() override { return Upstream->RunFrame(); }
	uint32 GetIPCCallCount() override { return Upstream->GetIPCCallCount(); }
	void SetWarningMessageHook(SteamAPIWarningMessageHook_t pFunction) override { return Upstream->SetWarningMessageHook(pFunction); }
	bool BShutdownIfAllPipesClosed() override { return Upstream->BShutdownIfAllPipesClosed(); }
	ISteamHTTP * GetISteamHTTP(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamHTTP(hSteamuser, hSteamPipe, pchVersion); }
	void * DEPRECATED_GetISteamUnifiedMessages(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->DEPRECATED_GetISteamUnifiedMessages(hSteamuser, hSteamPipe, pchVersion); }
	ISteamController * GetISteamController(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamController(hSteamUser, hSteamPipe, pchVersion); }
	ISteamUGC * GetISteamUGC(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUGC(hSteamUser, hSteamPipe, pchVersion); }
	ISteamAppList * GetISteamAppList(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamAppList(hSteamUser, hSteamPipe, pchVersion); }
	ISteamMusic * GetISteamMusic(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMusic(hSteamuser, hSteamPipe, pchVersion); }
	ISteamMusicRemote * GetISteamMusicRemote(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMusicRemote(hSteamuser, hSteamPipe, pchVersion); }
	ISteamHTMLSurface * GetISteamHTMLSurface(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamHTMLSurface(hSteamuser, hSteamPipe, pchVersion); }
	void DEPRECATED_Set_SteamAPI_CPostAPIResultInProcess(void ( *arg0 )(  )) override { return Upstream->DEPRECATED_Set_SteamAPI_CPostAPIResultInProcess(arg0); }
	void DEPRECATED_Remove_SteamAPI_CPostAPIResultInProcess(void ( *arg0 )(  )) override { return Upstream->DEPRECATED_Remove_SteamAPI_CPostAPIResultInProcess(arg0); }
	void Set_SteamAPI_CCheckCallbackRegisteredInProcess(SteamAPI_CheckCallbackRegistered_t func) override { return Upstream->Set_SteamAPI_CCheckCallbackRegisteredInProcess(func); }
	ISteamInventory * GetISteamInventory(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamInventory(hSteamuser, hSteamPipe, pchVersion); }
	ISteamVideo * GetISteamVideo(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamVideo(hSteamuser, hSteamPipe, pchVersion); }
	ISteamParentalSettings * GetISteamParentalSettings(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamParentalSettings(hSteamuser, hSteamPipe, pchVersion); }
};

class ProxySteamClient018: public ISteamClient018 {
protected:
	ISteamClient018* Upstream;

public:
	ProxySteamClient018(ISteamClient018* Upstream):
		Upstream{ Upstream }
	{}

	HSteamPipe CreateSteamPipe() override { return Upstream->CreateSteamPipe(); }
	bool BReleaseSteamPipe(HSteamPipe hSteamPipe) override { return Upstream->BReleaseSteamPipe(hSteamPipe); }
	HSteamUser ConnectToGlobalUser(HSteamPipe hSteamPipe) override { return Upstream->ConnectToGlobalUser(hSteamPipe); }
	HSteamUser CreateLocalUser(HSteamPipe * phSteamPipe, EAccountType eAccountType) override { return Upstream->CreateLocalUser(phSteamPipe, eAccountType); }
	void ReleaseUser(HSteamPipe hSteamPipe, HSteamUser hUser) override { return Upstream->ReleaseUser(hSteamPipe, hUser); }
	ISteamUser * GetISteamUser(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUser(hSteamUser, hSteamPipe, pchVersion); }
	ISteamGameServer * GetISteamGameServer(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGameServer(hSteamUser, hSteamPipe, pchVersion); }
	void SetLocalIPBinding(uint32 unIP, uint16 usPort) override { return Upstream->SetLocalIPBinding(unIP, usPort); }
	ISteamFriends * GetISteamFriends(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamFriends(hSteamUser, hSteamPipe, pchVersion); }
	ISteamUtils * GetISteamUtils(HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUtils(hSteamPipe, pchVersion); }
	ISteamMatchmaking * GetISteamMatchmaking(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMatchmaking(hSteamUser, hSteamPipe, pchVersion); }
	ISteamMatchmakingServers * GetISteamMatchmakingServers(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMatchmakingServers(hSteamUser, hSteamPipe, pchVersion); }
	void * GetISteamGenericInterface(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGenericInterface(hSteamUser, hSteamPipe, pchVersion); }
	ISteamUserStats * GetISteamUserStats(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUserStats(hSteamUser, hSteamPipe, pchVersion); }
	ISteamGameServerStats * GetISteamGameServerStats(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGameServerStats(hSteamuser, hSteamPipe, pchVersion); }
	ISteamApps * GetISteamApps(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamApps(hSteamUser, hSteamPipe, pchVersion); }
	ISteamNetworking * GetISteamNetworking(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamNetworking(hSteamUser, hSteamPipe, pchVersion); }
	ISteamRemoteStorage * GetISteamRemoteStorage(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamRemoteStorage(hSteamuser, hSteamPipe, pchVersion); }
	ISteamScreenshots * GetISteamScreenshots(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamScreenshots(hSteamuser, hSteamPipe, pchVersion); }
	ISteamGameSearch * GetISteamGameSearch(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGameSearch(hSteamuser, hSteamPipe, pchVersion); }
	void RunFrame() override { return Upstream->RunFrame(); }
	uint32 GetIPCCallCount() override { return Upstream->GetIPCCallCount(); }
	void SetWarningMessageHook(SteamAPIWarningMessageHook_t pFunction) override { return Upstream->SetWarningMessageHook(pFunction); }
	bool BShutdownIfAllPipesClosed() override { return Upstream->BShutdownIfAllPipesClosed(); }
	ISteamHTTP * GetISteamHTTP(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamHTTP(hSteamuser, hSteamPipe, pchVersion); }
	void * DEPRECATED_GetISteamUnifiedMessages(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->DEPRECATED_GetISteamUnifiedMessages(hSteamuser, hSteamPipe, pchVersion); }
	ISteamController * GetISteamController(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamController(hSteamUser, hSteamPipe, pchVersion); }
	ISteamUGC * GetISteamUGC(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUGC(hSteamUser, hSteamPipe, pchVersion); }
	ISteamAppList * GetISteamAppList(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamAppList(hSteamUser, hSteamPipe, pchVersion); }
	ISteamMusic * GetISteamMusic(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMusic(hSteamuser, hSteamPipe, pchVersion); }
	ISteamMusicRemote * GetISteamMusicRemote(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMusicRemote(hSteamuser, hSteamPipe, pchVersion); }
	ISteamHTMLSurface * GetISteamHTMLSurface(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamHTMLSurface(hSteamuser, hSteamPipe, pchVersion); }
	void DEPRECATED_Set_SteamAPI_CPostAPIResultInProcess(void ( *arg0 )(  )) override { return Upstream->DEPRECATED_Set_SteamAPI_CPostAPIResultInProcess(arg0); }
	void DEPRECATED_Remove_SteamAPI_CPostAPIResultInProcess(void ( *arg0 )(  )) override { return Upstream->DEPRECATED_Remove_SteamAPI_CPostAPIResultInProcess(arg0); }
	void Set_SteamAPI_CCheckCallbackRegisteredInProcess(SteamAPI_CheckCallbackRegistered_t func) override { return Upstream->Set_SteamAPI_CCheckCallbackRegisteredInProcess(func); }
	ISteamInventory * GetISteamInventory(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamInventory(hSteamuser, hSteamPipe, pchVersion); }
	ISteamVideo * GetISteamVideo(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamVideo(hSteamuser, hSteamPipe, pchVersion); }
	ISteamParentalSettings * GetISteamParentalSettings(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamParentalSettings(hSteamuser, hSteamPipe, pchVersion); }
	ISteamInput * GetISteamInput(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamInput(hSteamUser, hSteamPipe, pchVersion); }
	ISteamParties * GetISteamParties(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamParties(hSteamUser, hSteamPipe, pchVersion); }
};

class ProxySteamClient019: public ISteamClient019 {
protected:
	ISteamClient019* Upstream;

public:
	ProxySteamClient019(ISteamClient019* Upstream):
		Upstream{ Upstream }
	{}

	HSteamPipe CreateSteamPipe() override { return Upstream->CreateSteamPipe(); }
	bool BReleaseSteamPipe(HSteamPipe hSteamPipe) override { return Upstream->BReleaseSteamPipe(hSteamPipe); }
	HSteamUser ConnectToGlobalUser(HSteamPipe hSteamPipe) override { return Upstream->ConnectToGlobalUser(hSteamPipe); }
	HSteamUser CreateLocalUser(HSteamPipe * phSteamPipe, EAccountType eAccountType) override { return Upstream->CreateLocalUser(phSteamPipe, eAccountType); }
	void ReleaseUser(HSteamPipe hSteamPipe, HSteamUser hUser) override { return Upstream->ReleaseUser(hSteamPipe, hUser); }
	ISteamUser * GetISteamUser(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUser(hSteamUser, hSteamPipe, pchVersion); }
	ISteamGameServer * GetISteamGameServer(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGameServer(hSteamUser, hSteamPipe, pchVersion); }
	void SetLocalIPBinding(uint32 unIP, uint16 usPort) override { return Upstream->SetLocalIPBinding(unIP, usPort); }
	ISteamFriends * GetISteamFriends(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamFriends(hSteamUser, hSteamPipe, pchVersion); }
	ISteamUtils * GetISteamUtils(HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUtils(hSteamPipe, pchVersion); }
	ISteamMatchmaking * GetISteamMatchmaking(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMatchmaking(hSteamUser, hSteamPipe, pchVersion); }
	ISteamMatchmakingServers * GetISteamMatchmakingServers(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMatchmakingServers(hSteamUser, hSteamPipe, pchVersion); }
	void * GetISteamGenericInterface(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGenericInterface(hSteamUser, hSteamPipe, pchVersion); }
	ISteamUserStats * GetISteamUserStats(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUserStats(hSteamUser, hSteamPipe, pchVersion); }
	ISteamGameServerStats * GetISteamGameServerStats(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGameServerStats(hSteamuser, hSteamPipe, pchVersion); }
	ISteamApps * GetISteamApps(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamApps(hSteamUser, hSteamPipe, pchVersion); }
	ISteamNetworking * GetISteamNetworking(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamNetworking(hSteamUser, hSteamPipe, pchVersion); }
	ISteamRemoteStorage * GetISteamRemoteStorage(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamRemoteStorage(hSteamuser, hSteamPipe, pchVersion); }
	ISteamScreenshots * GetISteamScreenshots(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamScreenshots(hSteamuser, hSteamPipe, pchVersion); }
	ISteamGameSearch * GetISteamGameSearch(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamGameSearch(hSteamuser, hSteamPipe, pchVersion); }
	void RunFrame() override { return Upstream->RunFrame(); }
	uint32 GetIPCCallCount() override { return Upstream->GetIPCCallCount(); }
	void SetWarningMessageHook(SteamAPIWarningMessageHook_t pFunction) override { return Upstream->SetWarningMessageHook(pFunction); }
	bool BShutdownIfAllPipesClosed() override { return Upstream->BShutdownIfAllPipesClosed(); }
	ISteamHTTP * GetISteamHTTP(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamHTTP(hSteamuser, hSteamPipe, pchVersion); }
	void * DEPRECATED_GetISteamUnifiedMessages(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->DEPRECATED_GetISteamUnifiedMessages(hSteamuser, hSteamPipe, pchVersion); }
	ISteamController * GetISteamController(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamController(hSteamUser, hSteamPipe, pchVersion); }
	ISteamUGC * GetISteamUGC(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamUGC(hSteamUser, hSteamPipe, pchVersion); }
	ISteamAppList * GetISteamAppList(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamAppList(hSteamUser, hSteamPipe, pchVersion); }
	ISteamMusic * GetISteamMusic(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMusic(hSteamuser, hSteamPipe, pchVersion); }
	ISteamMusicRemote * GetISteamMusicRemote(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamMusicRemote(hSteamuser, hSteamPipe, pchVersion); }
	ISteamHTMLSurface * GetISteamHTMLSurface(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamHTMLSurface(hSteamuser, hSteamPipe, pchVersion); }
	void DEPRECATED_Set_SteamAPI_CPostAPIResultInProcess(void ( *arg0 )(  )) override { return Upstream->DEPRECATED_Set_SteamAPI_CPostAPIResultInProcess(arg0); }
	void DEPRECATED_Remove_SteamAPI_CPostAPIResultInProcess(void ( *arg0 )(  )) override { return Upstream->DEPRECATED_Remove_SteamAPI_CPostAPIResultInProcess(arg0); }
	void Set_SteamAPI_CCheckCallbackRegisteredInProcess(SteamAPI_CheckCallbackRegistered_t func) override { return Upstream->Set_SteamAPI_CCheckCallbackRegisteredInProcess(func); }
	ISteamInventory * GetISteamInventory(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamInventory(hSteamuser, hSteamPipe, pchVersion); }
	ISteamVideo * GetISteamVideo(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamVideo(hSteamuser, hSteamPipe, pchVersion); }
	ISteamParentalSettings * GetISteamParentalSettings(HSteamUser hSteamuser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamParentalSettings(hSteamuser, hSteamPipe, pchVersion); }
	ISteamInput * GetISteamInput(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamInput(hSteamUser, hSteamPipe, pchVersion); }
	ISteamParties * GetISteamParties(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamParties(hSteamUser, hSteamPipe, pchVersion); }
	ISteamRemotePlay * GetISteamRemotePlay(HSteamUser hSteamUser, HSteamPipe hSteamPipe, char const * pchVersion) override { return Upstream->GetISteamRemotePlay(hSteamUser, hSteamPipe, pchVersion); }
};

class ProxySteamApps007: public ISteamApps007 {
protected:
	ISteamApps007* Upstream;

public:
	ProxySteamApps007(ISteamApps007* Upstream):
		Upstream{ Upstream }
	{}

	bool BIsSubscribed() override { return Upstream->BIsSubscribed(); }
	bool BIsLowViolence() override { return Upstream->BIsLowViolence(); }
	bool BIsCybercafe() override { return Upstream->BIsCybercafe(); }
	bool BIsVACBanned() override { return Upstream->BIsVACBanned(); }
	char const * GetCurrentGameLanguage() override { return Upstream->GetCurrentGameLanguage(); }
	char const * GetAvailableGameLanguages() override { return Upstream->GetAvailableGameLanguages(); }
	bool BIsSubscribedApp(AppId_t appID) override { return Upstream->BIsSubscribedApp(appID); }
	bool BIsDlcInstalled(AppId_t appID) override { return Upstream->BIsDlcInstalled(appID); }
	uint32 GetEarliestPurchaseUnixTime(AppId_t nAppID) override { return Upstream->GetEarliestPurchaseUnixTime(nAppID); }
	bool BIsSubscribedFromFreeWeekend() override { return Upstream->BIsSubscribedFromFreeWeekend(); }
	int GetDLCCount() override { return Upstream->GetDLCCount(); }
	bool BGetDLCDataByIndex(int iDLC, AppId_t * pAppID, bool * pbAvailable, char * pchName, int cchNameBufferSize) override { return Upstream->BGetDLCDataByIndex(iDLC, pAppID, pbAvailable, pchName, cchNameBufferSize); }
	void InstallDLC(AppId_t nAppID) override { return Upstream->InstallDLC(nAppID); }
	void UninstallDLC(AppId_t nAppID) override { return Upstream->UninstallDLC(nAppID); }
	void RequestAppProofOfPurchaseKey(AppId_t nAppID) override { return Upstream->RequestAppProofOfPurchaseKey(nAppID); }
	bool GetCurrentBetaName(char * pchName, int cchNameBufferSize) override { return Upstream->GetCurrentBetaName(pchName, cchNameBufferSize); }
	bool MarkContentCorrupt(bool bMissingFilesOnly) override { return Upstream->MarkContentCorrupt(bMissingFilesOnly); }
	uint32 GetInstalledDepots(AppId_t appID, DepotId_t * pvecDepots, uint32 cMaxDepots) override { return Upstream->GetInstalledDepots(appID, pvecDepots, cMaxDepots); }
	uint32 GetAppInstallDir(AppId_t appID, char * pchFolder, uint32 cchFolderBufferSize) override { return Upstream->GetAppInstallDir(appID, pchFolder, cchFolderBufferSize); }
	bool BIsAppInstalled(AppId_t appID) override { return Upstream->BIsAppInstalled(appID); }
	CSteamID GetAppOwner() override { return Upstream->GetAppOwner(); }
	char const * GetLaunchQueryParam(char const * pchKey) override { return Upstream->GetLaunchQueryParam(pchKey); }
	bool GetDlcDownloadProgress(AppId_t nAppID, uint64 * punBytesDownloaded, uint64 * punBytesTotal) override { return Upstream->GetDlcDownloadProgress(nAppID, punBytesDownloaded, punBytesTotal); }
	int GetAppBuildId() override { return Upstream->GetAppBuildId(); }
};

class ProxySteamApps006: public ISteamApps006 {
protected:
	ISteamApps006* Upstream;

public:
	ProxySteamApps006(ISteamApps006* Upstream):
		Upstream{ Upstream }
	{}

	bool BIsSubscribed() override { return Upstream->BIsSubscribed(); }
	bool BIsLowViolence() override { return Upstream->BIsLowViolence(); }
	bool BIsCybercafe() override { return Upstream->BIsCybercafe(); }
	bool BIsVACBanned() override { return Upstream->BIsVACBanned(); }
	char const * GetCurrentGameLanguage() override { return Upstream->GetCurrentGameLanguage(); }
	char const * GetAvailableGameLanguages() override { return Upstream->GetAvailableGameLanguages(); }
	bool BIsSubscribedApp(AppId_t appID) override { return Upstream->BIsSubscribedApp(appID); }
	bool BIsDlcInstalled(AppId_t appID) override { return Upstream->BIsDlcInstalled(appID); }
	uint32 GetEarliestPurchaseUnixTime(AppId_t nAppID) override { return Upstream->GetEarliestPurchaseUnixTime(nAppID); }
	bool BIsSubscribedFromFreeWeekend() override { return Upstream->BIsSubscribedFromFreeWeekend(); }
	int GetDLCCount() override { return Upstream->GetDLCCount(); }
	bool BGetDLCDataByIndex(int iDLC, AppId_t * pAppID, bool * pbAvailable, char * pchName, int cchNameBufferSize) override { return Upstream->BGetDLCDataByIndex(iDLC, pAppID, pbAvailable, pchName, cchNameBufferSize); }
	void InstallDLC(AppId_t nAppID) override { return Upstream->InstallDLC(nAppID); }
	void UninstallDLC(AppId_t nAppID) override { return Upstream->UninstallDLC(nAppID); }
	void RequestAppProofOfPurchaseKey(AppId_t nAppID) override { return Upstream->RequestAppProofOfPurchaseKey(nAppID); }
	bool GetCurrentBetaName(char * pchName, int cchNameBufferSize) override { return Upstream->GetCurrentBetaName(pchName, cchNameBufferSize); }
	bool MarkContentCorrupt(bool bMissingFilesOnly) override { return Upstream->MarkContentCorrupt(bMissingFilesOnly); }
	uint32 GetInstalledDepots(AppId_t appID, DepotId_t * pvecDepots, uint32 cMaxDepots) override { return Upstream->GetInstalledDepots(appID, pvecDepots, cMaxDepots); }
	uint32 GetAppInstallDir(AppId_t appID, char * pchFolder, uint32 cchFolderBufferSize) override { return Upstream->GetAppInstallDir(appID, pchFolder, cchFolderBufferSize); }
	bool BIsAppInstalled(AppId_t appID) override { return Upstream->BIsAppInstalled(appID); }
	CSteamID GetAppOwner() override { return Upstream->GetAppOwner(); }
	char const * GetLaunchQueryParam(char const * pchKey) override { return Upstream->GetLaunchQueryParam(pchKey); }
};

class ProxySteamApps005: public ISteamApps005 {
protected:
	ISteamApps005* Upstream;

public:
	ProxySteamApps005(ISteamApps005* Upstream):
		Upstream{ Upstream }
	{}

	bool BIsSubscribed() override { return Upstream->BIsSubscribed(); }
	bool BIsLowViolence() override { return Upstream->BIsLowViolence(); }
	bool BIsCybercafe() override { return Upstream->BIsCybercafe(); }
	bool BIsVACBanned() override { return Upstream->BIsVACBanned(); }
	char const * GetCurrentGameLanguage() override { return Upstream->GetCurrentGameLanguage(); }
	char const * GetAvailableGameLanguages() override { return Upstream->GetAvailableGameLanguages(); }
	bool BIsSubscribedApp(AppId_t appID) override { return Upstream->BIsSubscribedApp(appID); }
	bool BIsDlcInstalled(AppId_t appID) override { return Upstream->BIsDlcInstalled(appID); }
	uint32 GetEarliestPurchaseUnixTime(AppId_t nAppID) override { return Upstream->GetEarliestPurchaseUnixTime(nAppID); }
	bool BIsSubscribedFromFreeWeekend() override { return Upstream->BIsSubscribedFromFreeWeekend(); }
	int GetDLCCount() override { return Upstream->GetDLCCount(); }
	bool BGetDLCDataByIndex(int iDLC, AppId_t * pAppID, bool * pbAvailable, char * pchName, int cchNameBufferSize) override { return Upstream->BGetDLCDataByIndex(iDLC, pAppID, pbAvailable, pchName, cchNameBufferSize); }
	void InstallDLC(AppId_t nAppID) override { return Upstream->InstallDLC(nAppID); }
	void UninstallDLC(AppId_t nAppID) override { return Upstream->UninstallDLC(nAppID); }
	void RequestAppProofOfPurchaseKey(AppId_t nAppID) override { return Upstream->RequestAppProofOfPurchaseKey(nAppID); }
	bool GetCurrentBetaName(char * pchName, int cchNameBufferSize) override { return Upstream->GetCurrentBetaName(pchName, cchNameBufferSize); }
	bool MarkContentCorrupt(bool bMissingFilesOnly) override { return Upstream->MarkContentCorrupt(bMissingFilesOnly); }
	uint32 GetInstalledDepots(DepotId_t * pvecDepots, uint32 cMaxDepots) override { return Upstream->GetInstalledDepots(pvecDepots, cMaxDepots); }
	uint32 GetAppInstallDir(AppId_t appID, char * pchFolder, uint32 cchFolderBufferSize) override { return Upstream->GetAppInstallDir(appID, pchFolder, cchFolderBufferSize); }
	bool BIsAppInstalled(AppId_t appID) override { return Upstream->BIsAppInstalled(appID); }
};

class ProxySteamApps004: public ISteamApps004 {
protected:
	ISteamApps004* Upstream;

public:
	ProxySteamApps004(ISteamApps004* Upstream):
		Upstream{ Upstream }
	{}

	bool BIsSubscribed() override { return Upstream->BIsSubscribed(); }
	bool BIsLowViolence() override { return Upstream->BIsLowViolence(); }
	bool BIsCybercafe() override { return Upstream->BIsCybercafe(); }
	bool BIsVACBanned() override { return Upstream->BIsVACBanned(); }
	char const * GetCurrentGameLanguage() override { return Upstream->GetCurrentGameLanguage(); }
	char const * GetAvailableGameLanguages() override { return Upstream->GetAvailableGameLanguages(); }
	bool BIsSubscribedApp(AppId_t appID) override { return Upstream->BIsSubscribedApp(appID); }
	bool BIsDlcInstalled(AppId_t appID) override { return Upstream->BIsDlcInstalled(appID); }
	uint32 GetEarliestPurchaseUnixTime(AppId_t nAppID) override { return Upstream->GetEarliestPurchaseUnixTime(nAppID); }
	bool BIsSubscribedFromFreeWeekend() override { return Upstream->BIsSubscribedFromFreeWeekend(); }
	int GetDLCCount() override { return Upstream->GetDLCCount(); }
	bool BGetDLCDataByIndex(int iDLC, AppId_t * pAppID, bool * pbAvailable, char * pchName, int cchNameBufferSize) override { return Upstream->BGetDLCDataByIndex(iDLC, pAppID, pbAvailable, pchName, cchNameBufferSize); }
	void InstallDLC(AppId_t nAppID) override { return Upstream->InstallDLC(nAppID); }
	void UninstallDLC(AppId_t nAppID) override { return Upstream->UninstallDLC(nAppID); }
};

class ProxySteamApps003: public ISteamApps003 {
protected:
	ISteamApps003* Upstream;

public:
	ProxySteamApps003(ISteamApps003* Upstream):
		Upstream{ Upstream }
	{}

	bool BIsSubscribed() override { return Upstream->BIsSubscribed(); }
	bool BIsLowViolence() override { return Upstream->BIsLowViolence(); }
	bool BIsCybercafe() override { return Upstream->BIsCybercafe(); }
	bool BIsVACBanned() override { return Upstream->BIsVACBanned(); }
	char const * GetCurrentGameLanguage() override { return Upstream->GetCurrentGameLanguage(); }
	char const * GetAvailableGameLanguages() override { return Upstream->GetAvailableGameLanguages(); }
	bool BIsSubscribedApp(AppId_t appID) override { return Upstream->BIsSubscribedApp(appID); }
	bool BIsDlcInstalled(AppId_t appID) override { return Upstream->BIsDlcInstalled(appID); }
};

class ProxySteamApps002: public ISteamApps002 {
protected:
	ISteamApps002* Upstream;

public:
	ProxySteamApps002(ISteamApps002* Upstream):
		Upstream{ Upstream }
	{}

	bool BIsSubscribed() override { return Upstream->BIsSubscribed(); }
	bool BIsLowViolence() override { return Upstream->BIsLowViolence(); }
	bool BIsCybercafe() override { return Upstream->BIsCybercafe(); }
	bool BIsVACBanned() override { return Upstream->BIsVACBanned(); }
	char const * GetCurrentGameLanguage() override { return Upstream->GetCurrentGameLanguage(); }
	char const * GetAvailableGameLanguages() override { return Upstream->GetAvailableGameLanguages(); }
	bool BIsSubscribedApp(AppId_t appID) override { return Upstream->BIsSubscribedApp(appID); }
};

class ProxySteamApps001: public ISteamApps001 {
protected:
	ISteamApps001* Upstream;

public:
	ProxySteamApps001(ISteamApps001* Upstream):
		Upstream{ Upstream }
	{}

	int GetAppData(AppId_t nAppID, char const * pchKey, char * pchValue, int cchValueMax) override { return Upstream->GetAppData(nAppID, pchKey, pchValue, cchValueMax); }
};

