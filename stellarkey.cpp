/* StellarKey: A DLC unlocker for Steam on Linux.
   
   Copyright 2022 The StellarKey Authors.
   
   This program is free software: you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the Free
   Software Foundation, either version 2 of the License, or (at your option)
   any later version.
   
   This program is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
   more details.
   
   You should have received a copy of the GNU General Public License along
   with this program. If not, see <https://www.gnu.org/licenses/>.
 */

/* This is a library meant to be used with `LD_PRELOAD`. It interposes the Steam
   API functions listed at the bottom of the main file. Each interposer function
   construct an implementation of one of the Steam API interfaces, such as
   `ISteamClient` or `ISteamApps`.
   
   The constructed object is of `Unlock<Proxy>` type, where `Proxy` is an
   automatically generated implementation that forwards each function call
   to the original library, and `Unlock<Proxy>` overrides a few functions of
   interest. To override another function, simply add it to the `Unlock` class
   template. To add an interface, add the prefix to `gen_proxies_cpp.py`, add a
   `STELLARKEY_INTERPOSE` for the legacy accessor, include the interface in
   the `StellarDispatcher`, and then override the desired functions.
   
   This library needs to run on the Steam Runtime, which is based on Ubuntu
   12.04 and includes GCC 5.4. Therefore we try to avoid relying on constructs
   that were not present in C++14 or in external libraries. GNU extensions are
   also avoided for portability.
 */

#include <cerrno>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <regex>
#include <typeinfo>
#include <vector>
#include <map>
#include <mutex>

// For interface version auto-detection.
#ifdef __gnu_linux__
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif  // _GNU_SOURCE
#include <link.h>
#define STELLARKEY_DL_ITERATE_PHDR
#else   // __gnu_linux__
#include <dlfcn.h>
#endif  // __gnu_linux__

// STEAM_API_NODLL makes all Steam interface member functions public.
#define STEAM_API_NODLL
#define SteamApps SteamSteamApps
#define SteamClient SteamSteamClient
#include "sdk_includes/steam_api.h"
#undef SteamApps
#undef SteamClient

// stringify the result of a macro expansion,
// see https://gcc.gnu.org/onlinedocs/cpp/Stringizing.html
#define STR(s) #s
#define STRING(s) STR(s)

using std::endl;

// Objects in an anonymous namespace are not exported in the shared library.
namespace {
// Forward declarations.
static std::fstream init_log();
const char* GuessVersion( const std::string hint );
void* StellarDispatcher( void* upstream, const char* version );
ISteamApps *StellarSteamApps( void *upstream, const char *version );
ISteamClient *StellarSteamClient( void *upstream, const char *version );

/* To log to a logfile, run with e.g. `STELLARKEY_LOG=/tmp/stellarkey.log`.
   The null-initialized fstream discards everything without formatting.
   TODO macro or template that compiles away to nothing.
 */
static std::fstream init_log() {
#ifdef STELLARKEY_LOG
    std::fstream logfile( STRING(STELLARKEY_LOG), std::ios::app);
#else
    std::fstream logfile(getenv("STELLARKEY_LOG"), std::ios::app);
#endif
    logfile << "[Init] StellarKey version " STRING(STELLARKEY_VERSION) << endl;
    
    return logfile;
}

std::fstream logfile = init_log();

struct Config {
    /* Configuration parser singleton. Constructing this class finds and parses
       the configuration files. The constructor is private and the single
       instance can be accessed using the `Config::Get()` function.
       
       For now we are searching for "DLC.txt" or "cream_api.ini" in a directory
       given as an environment variable, or cwd if this variable is empty, and
       inside a "steam_settings" directory if no configuration files were found
       outside.
       
       The configuration format for DLC is very lenient and will accept any line
       of the form:
       
           12345 = "DLC name"
       
       Where all whitespace surrounding the line or the equals sign is ignored,
       the name might be empty and the quotation marks are optional. Any line
       that does not start with a number followed by '=' is silently ignored.
     */
    struct DLC {
        const AppId_t id;
        const std::string name;
    };
    std::vector<DLC> dlcs;
    bool unlock_all = true;
    
    static const Config* Get() {
        static const Config config;
        return &config;
    }
    
    private:
    explicit Config() {
        const char* env_var = getenv("STELLARKEY_DIR");
        const std::string config_dir = env_var and *env_var ? env_var : ".";
        logfile << "[Config] Searching for configuration files in "<< config_dir << endl;
        
        // TODO search wherever libsteam_api.so and/or libstellarkey.so are.
        if (!InitConfig(config_dir) or InitConfig(config_dir + "/steam_settings")) {
            logfile << "[Config] Did not find any configuration files." << endl;
        }
    }
    
    bool InitConfig(std::string config_dir) {
        // Add more files in the future when they are needed.
        bool found_dlc = ParseDlc(config_dir + "/DLC.txt") or ParseDlc(config_dir + "/cream_api.ini");
        return found_dlc; // or found_something_else or ...
    }
    
    bool ParseDlc(std::string filename) {
        std::ifstream dlc_file(filename);
        if (!dlc_file.is_open()) {
            logfile << "[Config] Failed to open configuration file "<< filename <<": "<< std::strerror(errno) << endl;
            return false;
        }
        logfile << "[Config] Parsing DLC file "<< filename << endl;
        
        const std::regex pattern(R""(\s*(\d+)\s*=\s*"?(.*?)"?\s*)"");
        for (std::string line; std::getline(dlc_file, line);) {
            std::smatch groups;
            if ( std::regex_match(line, groups, pattern) ) {
                logfile << "[Config] Adding DLC "<< groups[1] <<" = "<< groups[2] << endl;
                dlcs.emplace_back(DLC{(AppId_t)stoul(groups[1]), groups[2]});
            }
        }
        
        unlock_all = false;
        return true;
    }
};

#include "proxies.cpp"
template <typename Proxy>
class Unlocked: public Proxy {
    /* This class inherits from any of the `Proxy` implementations of the
       Steam interfaces and overrides the functions that interest us.
       Objects of this class should be retrieved using the `memo()` function.
     */
    
    // constructor
    using Proxy::Proxy;
    using Proxy::Upstream;
    using Upstream_t = decltype(Upstream);
    
    public:
    static Upstream_t memo( const Upstream_t upstream ) {
        /* Memoizer for unlocker objects. The upstream interfaces have no
           destructor, so `Unlocked` objects are leaked if they are constructed
           every time a game calls one of the accessors.
         */
        static std::mutex cache_mutex;
        const std::lock_guard<std::mutex> lock(cache_mutex);
        static std::map<Upstream_t, Unlocked<Proxy>> cache;
        
        logfile << "[StellarKey] Proxy for "<< typeid(Upstream_t).name()<<" at "<< upstream;
        
        if (!upstream) {
            logfile << " is null!" << endl;
            return nullptr;
        }
        
        const auto result = cache.emplace(upstream, upstream);
        Unlocked<Proxy>* unlocked = &result.first->second;
        const bool was_not_cached = result.second;
        
        if (was_not_cached) {
            logfile << " created at " << unlocked << endl;
        } else {
            logfile << " found at " << unlocked << endl;
        }
        
        return unlocked;
    }
    
    bool BIsDlcInstalled( AppId_t appID ) {
        logfile << "[StellarKey] BIsDlcInstalled(appID = " << appID << ")" << endl;
        const Config* c = Config::Get();
        if (c->unlock_all) return true;
        
        for (const auto& dlc : c->dlcs) {
            if (dlc.id == appID) return true;
        }
        
        logfile << "[StellarKey] Not unlocking DLC " << appID << endl;
        return false;
    }
    
    int GetDLCCount() {
        logfile << "[StellarKey] GetDLCCount()" << endl;
        const Config* c = Config::Get();
        int ret;
        
        if (c->unlock_all) {
            ret = Upstream->GetDLCCount();
            logfile << "[StellarKey] Upstream returned GetDLCCount = "<< ret << endl;
        } else {
            ret = c->dlcs.size();
        }
        return ret;
    }
    
    bool BGetDLCDataByIndex( int iDLC, AppId_t *pAppID, bool *pbAvailable, char *pchName, int cchNameBufferSize ) {
        // *pbAvailable means that the DLC is available to buy, not that it is "owned".
        logfile << "[StellarKey] BGetDLCDataByIndex(iDLC = "<< iDLC <<", …, cchNameBufferSize = "<< cchNameBufferSize <<")" << endl;
        const Config* c = Config::Get();
        
        if (c->unlock_all) {
            bool ret = Upstream->BGetDLCDataByIndex( iDLC, pAppID, pbAvailable, pchName, cchNameBufferSize );
            logfile << "[StellarKey] Upstream returned BGetDLCDataByIndex = "<< ret <<", pAppID = "<< *pAppID <<", pbAvailable = "<< *pbAvailable <<", pchName = "<< pchName << endl;
            return ret;
        } else if (iDLC < 0 or (size_t)iDLC >= c->dlcs.size()) {
            logfile << "[StellarKey] BGetDLCDataByIndex("<< iDLC <<") out of range "<< c->dlcs.size() << endl;
            return false;
        } else {
            const auto dlc = c->dlcs[iDLC];
            
            if (pAppID) *pAppID = dlc.id;
            if (pbAvailable) *pbAvailable = true;
            if (pchName and cchNameBufferSize > 0) {
                strncpy(pchName, dlc.name.c_str(), cchNameBufferSize);
                pchName[cchNameBufferSize-1] = 0;
            }
            
            logfile << "[StellarKey] BGetDLCDataByIndex pAppID = "<< *pAppID <<", pbAvailable = "<< *pbAvailable <<", pchName = "<< pchName << endl;
            return true;
        }
    }
    
    bool BIsSubscribedApp( AppId_t appID ) {
        logfile << "[StellarKey] BIsSubscribedApp(appID = " << appID << ")" << endl;
        const Config* c = Config::Get();
        if (c->unlock_all) return true;
        
        for (const auto& dlc : c->dlcs) {
            if (dlc.id == appID) return true;
        }
        
        logfile << "[StellarKey] Not unlocking DLC " << appID << endl;
        return false;
    }
    
    
    ISteamApps *GetISteamApps( HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char *pchVersion ) {
        logfile << "[StellarKey] GetISteamApps(hSteamUser = "<<hSteamUser<<", hSteamPipe = "<<hSteamPipe<<", pchVersion = "<<pchVersion<<")" << endl;
        ISteamApps* realSteamApps = Upstream->GetISteamApps(hSteamUser, hSteamPipe, pchVersion);
        if (!realSteamApps) {
            logfile << "[StellarKey] Real GetISteamApps() returned NULL" << endl;
            return nullptr;
        }
        
        return StellarSteamApps(realSteamApps, pchVersion);
    }
};

const char* GuessVersion( const std::string hint ) {
    /* This function attempts to guess which version of a Steam interface the
       game is requesting when it calls one  of the unversioned accessors. This
       information can only be found in the Steam API shared library object.
       This is inevitably platform-dependent and gated with macros.
       
       Since some games call the accessors very frequenty, and this can be a
       rather slow operation, the results are cached by the "hint". It might be
       a better idea to cache by the address of the interface pointer, in case
       there's some game out there with multiple versions of the Steam API
       shared object linked in the same process.
       
       In GNU/Linux we use glibc introspection to find the first shared object
       in memory whose name ends in `libsteam_api.so`, and search for the
       interface version string in the loaded sections. An alternative
       implementation for Linux could inspect something like `/proc/self/maps`
       and search for the strings in the file. However procfs might not be
       available in a sandboxed process.
       
       The fallback implementation guesses the last widespread versions of the
       interfaces that had unversioned accessors. Newer versions seem to be
       always versioned.
     */
#ifdef STELLARKEY_DL_ITERATE_PHDR
    static std::mutex cache_mutex;
    const std::lock_guard<std::mutex> lock(cache_mutex);
    static std::map<const std::string, std::string> cache;
    
    if (!cache[hint].empty()) {
        logfile << "[Guess] Found cached version "<< cache[hint] <<" for interface "<< hint << endl;
        return cache[hint].c_str();
    }
    logfile << "[Guess] Searching version for interface "<< hint << endl;
    
    std::string data = hint;
    bool found = dl_iterate_phdr([](struct dl_phdr_info *info, size_t /* size */, void* data_) {
        /* For each shared object (library) linked in the program.
           `info->dlpi_addr`: Base address of the object in memory.
           `info->dlpi_name`: Pathname of the shared object (empty string for main).
           `info->dlpi_phdr[info->dlpi_phnum]`: ELF segment headers.
           
           These program headers are structures of the following form:
           
               typedef struct {
                   Elf32_Word  p_type;    // Segment type
                   Elf32_Off   p_offset;  // Segment file offset
                   Elf32_Addr  p_vaddr;   // Segment virtual address (relative to base)
                   Elf32_Addr  p_paddr;   // Segment physical address
                   Elf32_Word  p_filesz;  // Segment size in file
                   Elf32_Word  p_memsz;   // Segment size in memory
                   Elf32_Word  p_flags;   // Segment flags
                   Elf32_Word  p_align;   // Segment alignment
               } Elf32_Phdr;
               
           See example in `man 3 dl_iterate_phdr`.
         */
        auto data = reinterpret_cast<std::string*>(data_);
        logfile << "[Guess] Current library path " << info->dlpi_name << endl;
        if ( !std::regex_match(info->dlpi_name, std::regex(R"(.*libsteam_api\.so)")) ) {
            return 0;    // continue dl_iterate_phdr
        }
        
        // The current library is "libsteam_api.so", now let's find a readable
        // segment loaded from the file.
        for (int j = 0; j < info->dlpi_phnum; j++) {
            const auto segment = &info->dlpi_phdr[j];
            logfile << "[Guess] Segment "<< j+1 <<"/"<< info->dlpi_phnum <<": "<< segment << endl;
            if (segment->p_type != PT_LOAD or (segment->p_flags & PF_R) == 0) {
                continue;
            }
            
            // If we got here this segment is suitable, now we search for the hint.
            const char* offset = (char const*) (info->dlpi_addr + segment->p_vaddr);
            const auto length = segment->p_memsz;
            logfile << "[Guess] Searching for "<< *data <<" at "<< (void*)offset <<" length "<< length << endl;
            
            const std::regex needle(*data + R"(\d{3}\0)");
            std::cmatch found;
            if ( std::regex_search(offset, offset + length, found, needle)) {
                data->assign(found[0]);
                return 1; // break dl_iterate_phdr
            }
        }
        return 0;
    }, &data);
    // … perhaps I should have used strings | grep.
    
    if (found) {
        logfile << "[Guess] Found interface version "<< data << endl;
        cache[hint] = data;
        return cache[hint].c_str();
    } else {
        return "GUESS_FAILED";
    }
#else   // STELLARKEY_DL_ITERATE_PHDR
    logfile << "[Guess] Guessing version for interface "<< hint << endl;
    if (hint == "STEAMAPPS_INTERFACE_VERSION") {
        return "STEAMAPPS_INTERFACE_VERSION007";
    } else if (hint == "SteamClient") {
        return "SteamClient017";
    } else {
        return "GUESS_FAILED";
    }
#endif  // STELLARKEY_DL_ITERATE_PHDR
}

void* StellarDispatcher( void* upstream, const char* version ) {
    if (strstr(version, "STEAMAPPS_INTERFACE_VERSION") == version) {
        return StellarSteamApps(upstream, version);
    } else if (strstr(version, "SteamClient") == version) {
        return StellarSteamClient(upstream, version);
    } else {
        logfile << "[Dispatcher] Passing through "<< version <<" at "<< upstream << endl;
        return upstream;
    }
}

ISteamApps* StellarSteamApps( void* upstream, const char *version ) {
    void* ret;
    
    if (!version) {
        version = GuessVersion("STEAMAPPS_INTERFACE_VERSION");
    }
    logfile << "[Dispatcher] Real SteamApps version "<< version <<" is at "<< upstream << endl;
    
    if (strstr(version, "STEAMAPPS_INTERFACE_VERSION") != version) {
        logfile << "[Dispatcher] Called "<< __func__ <<" with unknown version string "<< version << endl;
        ret = upstream;
    } else if (strcmp(version, "STEAMAPPS_INTERFACE_VERSION002") == 0) {
        ret = Unlocked<ProxySteamApps002>::memo((ISteamApps002*) upstream);
    } else if (strcmp(version, "STEAMAPPS_INTERFACE_VERSION003") == 0) {
        ret = Unlocked<ProxySteamApps003>::memo((ISteamApps003*) upstream);
    } else if (strcmp(version, "STEAMAPPS_INTERFACE_VERSION004") == 0) {
        ret = Unlocked<ProxySteamApps004>::memo((ISteamApps004*) upstream);
    } else if (strcmp(version, "STEAMAPPS_INTERFACE_VERSION005") == 0) {
        ret = Unlocked<ProxySteamApps005>::memo((ISteamApps005*) upstream);
    } else if (strcmp(version, "STEAMAPPS_INTERFACE_VERSION006") == 0) {
        ret = Unlocked<ProxySteamApps006>::memo((ISteamApps006*) upstream);
    } else if (strcmp(version, "STEAMAPPS_INTERFACE_VERSION007") == 0) {
        ret = Unlocked<ProxySteamApps007>::memo((ISteamApps007*) upstream);
    } else {
        ret = Unlocked<ProxySteamApps>::memo((ISteamApps*) upstream);
    }
    
    return reinterpret_cast<ISteamApps*>(ret);
}

ISteamClient* StellarSteamClient( void* upstream, const char *version ) {
    void* ret;
    
    if (!version) {
        version = GuessVersion("SteamClient");
    }
    logfile << "[Dispatcher] Real SteamClient version "<< version <<" is at "<< upstream << endl;

    if (strstr(version, "SteamClient") != version) {
        logfile << "[Dispatcher] Called "<< __func__ <<" with unknown version string "<< version << endl;
        ret = upstream;
    } else if (strcmp(version, "SteamClient007") == 0) {
        ret = Unlocked<ProxySteamClient007>::memo((ISteamClient007*) upstream);
    } else if (strcmp(version, "SteamClient008") == 0) {
        ret = Unlocked<ProxySteamClient008>::memo((ISteamClient008*) upstream);
    } else if (strcmp(version, "SteamClient009") == 0) {
        ret = Unlocked<ProxySteamClient009>::memo((ISteamClient009*) upstream);
    } else if (strcmp(version, "SteamClient010") == 0) {
        ret = Unlocked<ProxySteamClient010>::memo((ISteamClient010*) upstream);
    } else if (strcmp(version, "SteamClient011") == 0) {
        ret = Unlocked<ProxySteamClient011>::memo((ISteamClient011*) upstream);
    } else if (strcmp(version, "SteamClient012") == 0) {
        ret = Unlocked<ProxySteamClient012>::memo((ISteamClient012*) upstream);
    } else if (strcmp(version, "SteamClient013") == 0) {
        ret = Unlocked<ProxySteamClient013>::memo((ISteamClient013*) upstream);
    } else if (strcmp(version, "SteamClient014") == 0) {
        ret = Unlocked<ProxySteamClient014>::memo((ISteamClient014*) upstream);
    } else if (strcmp(version, "SteamClient015") == 0) {
        ret = Unlocked<ProxySteamClient015>::memo((ISteamClient015*) upstream);
    } else if (strcmp(version, "SteamClient016") == 0) {
        ret = Unlocked<ProxySteamClient016>::memo((ISteamClient016*) upstream);
    } else if (strcmp(version, "SteamClient017") == 0) {
        ret = Unlocked<ProxySteamClient017>::memo((ISteamClient017*) upstream);
    } else if (strcmp(version, "SteamClient018") == 0) {
        ret = Unlocked<ProxySteamClient018>::memo((ISteamClient018*) upstream);
    } else if (strcmp(version, "SteamClient019") == 0) {
        ret = Unlocked<ProxySteamClient019>::memo((ISteamClient019*) upstream);
    } else {
        ret = Unlocked<ProxySteamClient>::memo((ISteamClient*) upstream);
    }
    
    return reinterpret_cast<ISteamClient*>(ret);
}

} // namespace {

// Exported functions:

#define STELLARKEY_INTERPOSE( SYMBOL, PARAMS, RET_EXPR )                        \
SYMBOL PARAMS {                                                                 \
    logfile << "[Interposer] " #SYMBOL << endl;                                 \
                                                                                \
    typedef decltype( & SYMBOL ) ThisFuncPointer;                               \
    /* making this static could cause problems if there ever is a game that     \
       `dlclose`s and then `dlopen`s the Steam API library. The upside is that  \
       we don't need to call `dlsym` every time a game calls the accessor. */   \
    static ThisFuncPointer upstream;                                            \
    if (!upstream) upstream = (ThisFuncPointer) dlsym(RTLD_NEXT, #SYMBOL );     \
    if (!upstream) upstream =                                                   \
        (ThisFuncPointer) dlsym(dlopen("libsteam_api.so", RTLD_LAZY), #SYMBOL );\
                                                                                \
    if (!upstream) {                                                            \
        logfile << "[Interposer] " #SYMBOL " not found: "<< dlerror() << endl;  \
        return nullptr;                                                         \
    }                                                                           \
                                                                                \
    return RET_EXPR;                                                            \
}                                                                               \

/* TODO I could not find a way to make the linker not emit tons of weak symbols
   in the dynamic table without GCC's `-fwhole-program` switch.
   
   The weak symbols were causing problems because some games export C++
   templates from a different `libstdc++` than ours, that are not compatible.
   
   `-Bsymbolic` helps with this except in the opposite case, where _we_
   accidentally interpose a game's incompatible template functions. Note that
   static libraries will export more symbols unless we use the
   `--exclude-libs ALL` linker option.
 */
#define SK_API S_API __attribute__((visibility("default"))) __attribute__((externally_visible))

SK_API void *S_CALLTYPE STELLARKEY_INTERPOSE(
    SteamInternal_FindOrCreateUserInterface,                        // SYMBOL
    ( HSteamUser hSteamUser, const char *pszVersion ),              // PARAMS
    StellarDispatcher(upstream(hSteamUser, pszVersion), pszVersion) // RET_EXPR
)

SK_API ISteamApps *S_CALLTYPE STELLARKEY_INTERPOSE(
    SteamApps,                                                      // SYMBOL
    (),                                                             // PARAMS
    StellarSteamApps(upstream(), nullptr)                           // RET_EXPR
)

SK_API void *S_CALLTYPE STELLARKEY_INTERPOSE(
    SteamInternal_CreateInterface,                                  // SYMBOL
    ( const char *ver ),                                            // PARAMS
    StellarSteamClient(upstream(ver), ver)                          // RET_EXPR
)

SK_API ISteamClient *S_CALLTYPE STELLARKEY_INTERPOSE(
    SteamClient,                                                    // SYMBOL
    (),                                                             // PARAMS
    StellarSteamClient(upstream(), nullptr)                         // RET_EXPR
)
